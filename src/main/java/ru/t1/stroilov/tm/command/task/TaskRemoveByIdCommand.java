package ru.t1.stroilov.tm.command.task;

import ru.t1.stroilov.tm.util.TerminalUtil;

public class TaskRemoveByIdCommand extends AbstractTaskCommand {

    public final static String DESCRIPTION = "Delete Task by ID.";

    public final static String NAME = "task-delete-by-id";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[DELETE TASK BY ID]");
        System.out.println("Enter id:");
        final String id = TerminalUtil.nextLine();
        getTaskService().deleteByID(getUserId(), id);
    }
}
