package ru.t1.stroilov.tm.command.task;

import ru.t1.stroilov.tm.util.TerminalUtil;

public class TaskRemoveByIndexCommand extends AbstractTaskCommand {

    public final static String DESCRIPTION = "Delete Task by index.";

    public final static String NAME = "task-delete-by-index";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[DELETE TASK BY INDEX]");
        System.out.println("Enter index:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        getTaskService().deleteByIndex(getUserId(), index);
    }
}
