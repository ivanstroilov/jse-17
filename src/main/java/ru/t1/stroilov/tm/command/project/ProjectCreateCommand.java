package ru.t1.stroilov.tm.command.project;

import ru.t1.stroilov.tm.util.TerminalUtil;

public class ProjectCreateCommand extends AbstractProjectCommand {

    public final static String DESCRIPTION = "Create a Project.";

    public final static String NAME = "project-create";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[CREATE PROJECT]");
        System.out.println("Enter name:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description:");
        final String description = TerminalUtil.nextLine();
        getProjectService().add(getUserId(), name, description);
    }
}
