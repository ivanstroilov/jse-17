package ru.t1.stroilov.tm.service;

import ru.t1.stroilov.tm.api.repository.IProjectRepository;
import ru.t1.stroilov.tm.api.repository.ITaskRepository;
import ru.t1.stroilov.tm.api.service.IProjectTaskService;
import ru.t1.stroilov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.stroilov.tm.exception.entity.TaskNotFoundException;
import ru.t1.stroilov.tm.exception.field.ProjectIdEmptyException;
import ru.t1.stroilov.tm.exception.field.TaskIdEmptyException;
import ru.t1.stroilov.tm.exception.field.UserIdEmptyException;
import ru.t1.stroilov.tm.model.Project;
import ru.t1.stroilov.tm.model.Task;

import java.util.List;

public final class ProjectTaskService implements IProjectTaskService {

    private final IProjectRepository projectRepository;

    private final ITaskRepository taskRepository;

    public ProjectTaskService(final IProjectRepository projectRepository, final ITaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public Task bindTaskToProject(final String userId, final String projectId, final String taskId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
        final Task task = taskRepository.findByID(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(projectId);
        return task;
    }

    @Override
    public void removeProjectById(final String userId, final String projectId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
        final List<Task> tasks = taskRepository.findAllByProjectID(userId, projectId);
        for (final Task task : tasks) taskRepository.deleteByID(task.getId());
        projectRepository.deleteByID(userId, projectId);
    }

    @Override
    public void removeProjects(final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        final List<Project> projects = projectRepository.findAll(userId);
        for (final Project project : projects) {
            if (project == null) continue;
            removeProjectById(userId, project.getId());
        }
    }

    @Override
    public Task unbindTaskFromProject(final String userId, final String projectId, final String taskId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (!projectRepository.existsById(projectId)) throw new ProjectNotFoundException();
        final Task task = taskRepository.findByID(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(null);
        return task;
    }
}
