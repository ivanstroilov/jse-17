package ru.t1.stroilov.tm.api.repository;

import ru.t1.stroilov.tm.model.Project;

public interface IProjectRepository extends IRepository<Project>, IUserOwnedRepository<Project> {

    Project add(String userId, String name, String description);

    Project add(String userId, String name);

}
