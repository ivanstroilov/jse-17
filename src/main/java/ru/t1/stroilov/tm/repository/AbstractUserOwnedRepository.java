package ru.t1.stroilov.tm.repository;

import ru.t1.stroilov.tm.api.repository.IUserOwnedRepository;
import ru.t1.stroilov.tm.enumerated.Sort;
import ru.t1.stroilov.tm.model.AbstractUserOwnedModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M> implements IUserOwnedRepository<M> {

    @Override
    public void deleteAll(final String userId) {
        final List<M> models = findAll(userId);
        deleteAll(models);
    }

    @Override
    public boolean existsById(String userId, String id) {
        return findByID(userId, id) != null;
    }

    @Override
    public List<M> findAll(String userId) {
        if (userId == null) return Collections.emptyList();
        final List<M> result = new ArrayList<>();
        for (final M model : models) {
            if (userId.equals(model.getUserId())) result.add(model);
        }
        return result;
    }

    @Override
    public List<M> findAll(String userId, Comparator<M> comparator) {
        final List<M> result = findAll(userId);
        result.sort(comparator);
        return result;
    }

    @Override
    public List<M> findAll(String userId, Sort sort) {
        if (userId == null) return null;
        if (sort == null) return findAll();
        final Comparator<M> comparator = (Comparator<M>) sort.getComparator();
        if (comparator == null) return findAll();
        return findAll(comparator);
    }

    @Override
    public M findByID(String userId, String id) {
        if (userId == null) return null;
        for (final M model : models) {
            if (!userId.equals(model.getUserId()) || !id.equals(model.getId())) continue;
            return model;
        }
        return null;
    }

    @Override
    public M findByIndex(String userId, Integer index) {
        if (userId == null) return null;
        M model = models.get(index);
        if (userId.equals(model.getUserId())) return model;
        return null;
    }

    @Override
    public int getSize(String userId) {
        if (userId == null) return 0;
        final List<M> result = findAll(userId);
        return result.size();
    }

    @Override
    public M deleteByID(String userId, String id) {
        if (userId == null) return null;
        final M model = findByID(userId, id);
        if (model == null) return null;
        return delete(model);
    }

    @Override
    public M deleteByIndex(String userId, Integer index) {
        if (userId == null) return null;
        final M model = findByIndex(userId, index);
        if (model == null) return null;
        return delete(model);
    }

    @Override
    public M add(String userId, M model) {
        if (userId == null) return null;
        model.setUserId(userId);
        models.add(model);
        return model;
    }

    @Override
    public M delete(String userId, M model) {
        if (userId == null) return null;
        if (model == null) return null;
        return deleteByID(userId, model.getId());
    }
}
